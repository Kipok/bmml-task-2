\documentclass{article}

\usepackage{fancyhdr} % Required for custom headers
\usepackage{lastpage} % Required to determine the last page for the footer
\usepackage{extramarks} % Required for headers and footers
\usepackage{graphicx} % Required to insert images
\usepackage[english,russian]{babel}
\usepackage[utf8]{inputenc}
\usepackage{amsmath,amssymb}
\usepackage{mathtools}

\DeclareMathOperator*{\argmax}{arg\,max}
\DeclareMathOperator*{\argmin}{arg\,min}
\DeclareMathOperator{\E}{\mathbb{E}}

\newcommand\norm[1]{\left\lVert#1\right\rVert}

% Margins
\topmargin=-0.45in
\evensidemargin=0in
\oddsidemargin=0in
\textwidth=6.5in
\textheight=9.0in
\headsep=0.25in

\linespread{1.1} % Line spacing

% Set up the header and footer
\pagestyle{fancy}
\lhead{\hmwkAuthorName} % Top left header
\rhead{\firstxmark} % Top right header
\lfoot{\lastxmark} % Bottom left footer
\cfoot{} % Bottom center footer
\rfoot{Page\ \thepage\ of\ \pageref{LastPage}} % Bottom right footer
\renewcommand\headrulewidth{0.4pt} % Size of the header rule
\renewcommand\footrulewidth{0.4pt} % Size of the footer rule

\setlength\parindent{0pt} % Removes all indentation from paragraphs

%----------------------------------------------------------------------------------------
%    DOCUMENT STRUCTURE COMMANDS
%    Skip this unless you know what you're doing
%----------------------------------------------------------------------------------------

% Header and footer for when a page split occurs within a problem environment
\newcommand{\enterProblemHeader}[1]{
\nobreak\extramarks{#1}{#1 continued on next page\ldots}\nobreak
\nobreak\extramarks{#1 (continued)}{#1 continued on next page\ldots}\nobreak
}

% Header and footer for when a page split occurs between problem environments
\newcommand{\exitProblemHeader}[1]{
\nobreak\extramarks{#1 (continued)}{#1 continued on next page\ldots}\nobreak
\nobreak\extramarks{#1}{}\nobreak
}

\setcounter{secnumdepth}{0} % Removes default section numbers
\newcounter{homeworkProblemCounter} % Creates a counter to keep track of the number of problems

\newcommand{\homeworkProblemName}{}
\newenvironment{homeworkProblem}[1][Problem \arabic{homeworkProblemCounter}]{ % Makes a new environment called homeworkProblem which takes 1 argument (custom name) but the default is "Problem #"
\stepcounter{homeworkProblemCounter} % Increase counter for number of problems
\renewcommand{\homeworkProblemName}{#1} % Assign \homeworkProblemName the name of the problem
\section{\homeworkProblemName} % Make a section in the document with the custom problem count
\enterProblemHeader{\homeworkProblemName} % Header and footer within the environment
}{
\exitProblemHeader{\homeworkProblemName} % Header and footer after the environment
}

\newcommand{\problemAnswer}[1]{ % Defines the problem answer command with the content as the only argument
\noindent\framebox[\columnwidth][c]{\begin{minipage}{0.98\columnwidth}#1\end{minipage}} % Makes the box around the problem answer and puts the content inside
}

\newcommand{\homeworkSectionName}{}
\newenvironment{homeworkSection}[1]{ % New environment for sections within homework problems, takes 1 argument - the name of the section
\renewcommand{\homeworkSectionName}{#1} % Assign \homeworkSectionName to the name of the section from the environment argument
\subsection{\homeworkSectionName} % Make a subsection with the custom name of the subsection
\enterProblemHeader{\homeworkProblemName\ [\homeworkSectionName]} % Header and footer within the environment
}{
\enterProblemHeader{\homeworkProblemName} % Header and footer after the environment
}

%----------------------------------------------------------------------------------------
%    NAME AND CLASS SECTION
%----------------------------------------------------------------------------------------

\newcommand{\hmwkTitle}{Задание\ №2 \\ <<EM алгоритм для детектива>>} % Assignment title
\newcommand{\hmwkClass}{Байесовские методы в машинном обучении,\ 417} % Course/class
\newcommand{\hmwkAuthorName}{Игорь Гитман} % Your name

%----------------------------------------------------------------------------------------
%    TITLE PAGE
%----------------------------------------------------------------------------------------

\title{
\vspace{2in}
\textmd{\textbf{\hmwkClass:\ \hmwkTitle}}\\
\vspace{3in}
}

\author{\textbf{\hmwkAuthorName}}
\date{} % Insert date here if you want it to appear below your name

%----------------------------------------------------------------------------------------

\begin{document}

\maketitle

%----------------------------------------------------------------------------------------
%    TABLE OF CONTENTS
%----------------------------------------------------------------------------------------

%\setcounter{tocdepth}{1} % Uncomment this line if you don't want subsections listed in the ToC

\newpage
\tableofcontents
\newpage

\begin{homeworkProblem}[Основное задание]
    Необходимо было:
    \begin{enumerate}
        \item Вывести все необходимые формулы.
        \item Протестировать полученный EM алгоритм на сгенерированных данных. Определить, сильно ли влияет начальное
            приближение на параметры на результаты работы? Стоит ли для данной задачи запускать ЕМ алгоритм из
            разных начальных приближений?
        \item Запустить ЕМ алгоритм на сгенерированных выборках разных размеров и с разным уровнем зашумления. Определить
            как изменения в обучающей выборке влияют на результаты работы (получаемые $F$, $B$ и $L(q, \theta, A))$? При
            каком уровне шума ЕМ-алгоритм перестает выдавать вменяемые результаты?
        \item Сравнить качество и время работы ЕМ и hard ЕМ на сгенерированных данных. Определить, почему
            разница в результатах работы так заметна?
        \item Применить ЕМ алгоритм к данным с зашумленными снимками преступника. Привести результаты работы
            алгоритма на выборках разного размера.
        \item Предложить какую-нибудь модификацию полученного ЕМ алгоритма, которая бы работала на данной
            задаче качественнее и/или быстрее.
    \end{enumerate}
\end{homeworkProblem}

\begin{homeworkProblem}[Вывод необходимых формул]
    Выведем формулу для апостериорного распределения:
    \begin{align*}
        p(d_k|X_k, \theta, A) = \frac{p(X_k | d_k, \theta) p(d_k | A)}{Z} =
                \frac{\mathcal{N}(X_k | \mu_k(d_k^h, d_k^w), s^2I) A(d_k^h, d_k^w)}{\sum_{i, j}\mathcal{N}(X_k | \mu_k(i, j), s^2I) A(i, j)}
    \end{align*}
    $\mu(i, j)$ --- это статичный фон $B$ с лицом $F$ в позиции $i, j$. Аргументом нормального распределения в данном случае является матрица, но мы рассматриваем ее
    как вектор --- в частности, в экспоненте вычисляется норма Фробениуса.

    Теперь посчитаем формулы для точечных оценок для параметров:
    \begin{align*}
        \E_{q(d)}&\log\left[p(X, d | \theta, A)\right] = \E_{q(d)}\sum_{k=1}^N\left[\log\mathcal{N}(X_k|\mu_k(d^h, d^w), s^2I) + \log A(d^h, d^w)\right] = \\
         &= \sum_{d^h, d^w}\sum_{k=1}^N q_k(d^h, d^w)\left[\log\mathcal{N}(X_k|\mu_k(d^h, d^w), s^2I) + \log A(d^h, d^w)\right] = \\
         &= f(B, F, s^2, A) \rightarrow \max_{B, F, s^2, A} \\
    \end{align*}
    Выведем формулы для матрицы $A$, используя условие $\sum_{ij}A_{ij} = 1$
    \begin{align*}
        \mathcal{L}(f, \lambda) &= f(A) + \lambda(\sum_{i,j}A_{ij} - 1) \\
        \frac{\partial \mathcal{L}}{\partial \lambda} &= \sum_{i, j}A_{ij} - 1 = 0 \\
        \frac{\partial \mathcal{L}}{\partial A_{ij}} &= \sum_k \frac{q_k(i, j)}{A_{ij}} + \lambda = 0 \Rightarrow \\
        \Rightarrow A_{ij} &= \frac{\sum_k q_k(i, j)}{\sum_{hw}\sum_k q_k(h, w)} = \frac{\sum_k q_k(i, j)}{N}
    \end{align*}
    Выведем формулы для матрицы $F$:
    \begin{align*}
        \frac{\partial f}{\partial F_{ij}} &= \sum_k \sum_{d^h, d^w} \frac{1}{s^2}\left[X_k(i + d^h, j + d^w) - F(i, j)\right] = 0 \Rightarrow \\
        \Rightarrow F_{ij} &= \frac{\sum_k\sum_{d^h, d^w} q_k(d^h, d^w)X_k(i + d^h, j + d^w)}{N}
    \end{align*}
    Выведем формулы для матрицы $B$:
    \begin{align*}
        \frac{\partial f}{\partial B_{ij}} &= \sum_k \sum_{d^h, d^w} \frac{1}{s^2}\left[X_k(i, j) - B(i, j)\right]\left[i \not\in [d^h, d^h+h], j \not\in [d^w, d^w + w]\right] = 0 \Rightarrow \\
        \Rightarrow B_{ij} &= \frac{\sum_k\sum_{d^h, d^w} q_k(d^h, d^w)X_k(i, j)\left[i \not\in [d^h, d^h+h], j \not\in [d^w, d^w + w]\right]}{\sum_k\sum_{d^h, d^w} q_k(d^h, d^w)\left[i \not\in [d^h, d^h+h], j \not\in [d^w, d^w + w]\right]}
    \end{align*}
    Выведем формулы для переменной $s$:
    \begin{align*}
        \frac{\partial f}{\partial s} &= \sum_k \sum_{d^h, d^w} -\frac{HW}{s} + \frac{||X_k - \mu(d^h, d^w)||^2}{s^3} = 0 \Rightarrow \\
        \Rightarrow s &= \sqrt{\frac{\sum_k\sum_{d^h, d^w} q_k(d^h, d^w)||X_k - \mu(d^h, d^w)||^2}{NHW}}
    \end{align*}
    В случае hard EM во всех формулах для точечных оценок исчезает сумма по $d^h,d^w$ и множитель $q(d^h,d^w)$, так как только для одной пары
    $d^h,d^w$ $q(d^h,d^w)$ будет равно $1$, а для остальных $0$. Все остальное останется идентичным (при каждом $k$ соответствующие $d^h,d^w$ нам известны с E-шага).

    И в конце, выведем формулы для нижней оценки на логарифм неполного правдоподобия:
    \begin{align*}
        \mathcal{L}(q, \theta, A) &= \E_{q(d)}\log p(X, d | \theta, A) - \E_{q(d)}\log q(d) =\\ &= \sum_k\sum_{d^h,d^w}q_k(d^h, d^w)\left[\log\mathcal{N}(X_k | \mu_k(d^h, d^w), s^2I) +  \log A(d^h, d^w) - \log q_k(d^h,d^w)\right]
    \end{align*}

\end{homeworkProblem}
\clearpage
\begin{homeworkProblem}[Запуск EM на сгенерированных данных]
    Для тестирования алгоритма использовались следующие модельные данные:
    \begin{center}
        \includegraphics[width=0.6\columnwidth]{images/modelB.png}\hspace{10pt}
        \includegraphics[width=0.28\columnwidth]{images/modelF.png}\vspace{10pt}
        \includegraphics[width=0.6\columnwidth]{images/noiseB.png}\hspace{10pt}
        \includegraphics[width=0.28\columnwidth]{images/foundF.png}
    \end{center}

    В верхнем ряду представлены исходные данные. Левая нижняя картинка --- пример зашумленного изображения, правая нижняя --- пример найденного лица для одного
    из запусков EM алгоритма (500 изображений в выборке, $s = \mu = 0.5$ --- параметры нормального распределения для генераци шума).

    Размеры изображений для модельного эксперимента были: $H = 25, W = 50, h = 16, w = 16$.

    Стоит заметить, что полученный алгоритм оказался довольно устойчивым к начальному приближению. Полученные результаты лишь незначительно различаются по уровню шума,
    если не ограничивать алгоритм по числу итераций, а работать до сходимости. Правда скорость сходимости от начального приближения уже сильно зависит.

 %   \begin{center}
 %       \includegraphics[width=0.45\columnwidth]{images/foundF1.png}
 %       \includegraphics[width=0.45\columnwidth]{images/foundF2.png}
 %   \end{center}
\end{homeworkProblem}
\clearpage
\begin{homeworkProblem}[Исследование зависимости результатов работы от размера и уровня зашумления обучающей выборки]
    Исследуем как зависят результаты работы EM-алгоритма от размеров обучающей выборки. Значения $L$ мы можем просто отобразить на графике, а вот с $F$ и $B$
    возникают проблемы. Довольно трудно автоматически сравнивать насколько удачными получились картинки. Различные метрики вроде $L_2$ или PSNR вряд ли будут
    адекватными задаче, так как иногда картинка, полученная на выходе очень похожа на исходную, но немного сдвинута на несколько пикселей. Ясно, что это гораздо
    лучше рандомного шума, но в случае упомянутых метрик значения будут примерно одинаковыми. Поэтому сравнения картинок проводились <<на глаз>>.
    \begin{center}
        \includegraphics[width=0.45\columnwidth]{images/size_plot.png}
        \includegraphics[width=0.45\columnwidth]{images/noise_plot.png}
    \end{center}
    На графиках представлены значения нижней оценки на логарифм неполного правдоподобия. Зеленым цветом обозначена ошибка (я усреднял по 5-10 результатов запуска
    EM-алгоритма). Как видно, зеленого цвета на графиках практически нет, что еще раз подтверждает, что в данной задаче EM-алгоритм в целом устойчив к различным
    начальным приближениям. Значения для левого графика получены при уровне шума в $0.5$, для правого
    при размере выборки в $100$ картинок. Видно, что в целом чем больше данных, и чем меньше шума, тем лучше получается оценка на правдоподобие. Некоторые отклонения
    от этой тенденции связаны с тем, что, все-таки каждый раз мы решаем немного разную задачу (получаем оценку на различные величины). Поэтому даже при увеличении
    количества картинок, результаты могут незначительно проседать. Также видно, что при размере выборки больше 500, нижняя оценка уже сильно не
    изменяется, что, предположительно означает, что мы получили практически точную оценку, и новые данные нам уже не нужны --- информации и так достаточно.

    Посмотрим теперь на картинки, полученные при разном размере исходной выборки:
    \begin{center}
        \includegraphics[width=0.27\columnwidth]{images/bad_size.png}\hspace{5pt}
        \includegraphics[width=0.27\columnwidth]{images/mid_size.png}\hspace{5pt}
        \includegraphics[width=0.27\columnwidth]{images/good_size.png}\vspace{10pt}
        \includegraphics[width=0.41\columnwidth]{images/b_size_bad.png}\hspace{10pt}
        \includegraphics[width=0.41\columnwidth]{images/b_size_good.png}
    \end{center}
    Слева направо показаны примеры картинок при очень маленьком (10-20), среднем (75-100) и очень большом (500-1000) диапазонах на размеры выборки.
    При очень маленьких размерах выборки, данных недостаточно, чтобы хоть сколько-нибудь хорошо оценить распределения, и поэтому объект выглядит практически рандомным
    шумом. Стоит заметить, что фон определяется значительно лучше: задача определения фона гораздо проще, ведь нам не нужно оценивать его положение, он статический.
    Поэтому даже при небольшом количестве данных фон определяется не плохо, хотя, конечно, и здесь шума очень много (в основном в центре, так как пиксели центра чаще
    всего оказывались перекрыты лицами и мы в них сильно не уверены).

    Для картинок с различным уровнем шума тенденции примерно такие же:

    \begin{center}
        \includegraphics[width=0.27\columnwidth]{images/bad_noise.png}\hspace{5pt}
        \includegraphics[width=0.27\columnwidth]{images/mid_noise.png}\hspace{5pt}
        \includegraphics[width=0.27\columnwidth]{images/good_noise.png}\vspace{10pt}
        \includegraphics[width=0.41\columnwidth]{images/b_noise_bad.png}\hspace{10pt}
        \includegraphics[width=0.41\columnwidth]{images/b_noise_good.png}
    \end{center}

    В целом при стандартном отклонении $s \approx 0.7-0.8$ картинки, уже определяются довольно плохо (средняя картинка). При значении $s = 1.0$ алгоритм уже не может
    ничего различить.
\end{homeworkProblem}
\clearpage
\begin{homeworkProblem}[Сравнение EM и hard EM]
    Сравним теперь качество и время работы EM и hard EM на модельных данных ($s = 0.5$).

    \begin{center}
        \includegraphics[width=0.95\columnwidth]{images/emhardem.png}
    \end{center}

    Видно, что, как ни странно, но в целом hard EM работает лучше --- он достигает того же качества за меньшее время. Правда при этом он значительно менее стабилен ---
    качество может существенно меняться от запуска к запуску. Хотя, конечно, сравнивать алгоритмы по полученной
    нижней оценке не совсем правильно. Если посмотреть на генерируемые картинки, то визуально качества hard EM на выборках больших размеров также идентично
    качеству стандартного алгоритма. А вот в случае выборок маленьких размеров, картинки для hard EM получаются визуально более шумными и иногда сдвинутыми
    на несколько пикселей.

%   Вероятно, задача получилась сильно простой и обощающей способности hard EM хватает, чтобы ее решить. Посмтрим, что будет, если мы усложним задачу, сильно увеличив
%   уровень шума.

%    \begin{center}
%        \includegraphics[width=0.95\columnwidth]{images/emhardem_noise.png}
%    \end{center}
\end{homeworkProblem}
\clearpage
\begin{homeworkProblem}[Применение EM алгоритма к реальным данным]
    Применим теперь EM алгоритм к данным с зашумленными снимками преступника. На полной выборке один запуск занимает примерно 20 минут, но так как алгоритм
    получился довольно устойчив к начальному приближению, одного запуска вполне достаточно, чтобы получить хорошее изображение лица преступника,
    и еще более хорошее изображение фона:

    \begin{center}
        \includegraphics[width=0.67\columnwidth]{images/realdataB.png}\hspace{10pt}
        \includegraphics[width=0.23\columnwidth]{images/realdataF.png}
    \end{center}

    Если использовать меньше данных, то алгоритм начинает работать значительно хуже: фон получается в целом различим, а вот от лица остаются только едва различимые
    очертания. Также стоит заметить, что чем меньше данных, тем больше EM-алгоритм зависит от начального приближения --- вероятно это связано с тем, что задача
    становится сильно сложнее.

    Пример полученных результатов для размера выборки в 100 картинок:
    \begin{center}
        \includegraphics[width=0.67\columnwidth]{images/realdataB_100.png}\hspace{10pt}
        \includegraphics[width=0.23\columnwidth]{images/realdataF_100.png}
    \end{center}

    Пример полученных результатов для размера выборки в 50 картинок:
    \begin{center}
        \includegraphics[width=0.67\columnwidth]{images/realdataB_50.png}\hspace{10pt}
        \includegraphics[width=0.23\columnwidth]{images/realdataF_50.png}
    \end{center}

\end{homeworkProblem}
\clearpage
\begin{homeworkProblem}[Модификация EM алгоритма для данной задачи]
    Так как в целом качество EM алгоритма для данной задачи вполне неплохое (изображения хорошо восстанавливаются лишь с небольшим уровнем шума), то разумно подумать
    про модификации, которые смогли бы ускорить алгоритм при сохранении того же уровня качества. Одним из вариантов может быть замена EM на hard EM.
    Другой возможной модификацией может быть использование стохастического EM-алгоритма --- на M-шаге каждый раз будем оптимизировать не честное мат. ожидание, а его
    несмещенную оценку (то есть, будем использовать не всю выборку, а лишь несколько картинок).

    Кроме того, можно подумать в сторону улучшения начального приближения (от него почти не зависит качество, но существенно зависит скорость сходимости). Вместо
    равномерно распределенных картинок в качестве начального приближения можно брать, например, нормально распределенные картинки с дисперсией и мат.ожаданием выборки.
    Возможно, это приближение окажется ближе к реальным данным и скорость сходимости увеличится.
\end{homeworkProblem}
\begin{homeworkProblem}[Общие выводы]
    В целом, видно, что EM-алгоритм является очень мощным инструментом, и позволяет решать довольно сложные задачи. Также из общих выводов можно отметить, что, когда
    задача относительно простая (достаточно данных, уровень шума относительно невысоких) --- hard EM работает не хуже, чем обычный EM алгоритм, но значительно быстрее.
    Кроме того, интересно, что для данной задачи EM-алгоритм получился весьма устойчив к начальному приближению, вероятно это опять же связано с ее относительной
    простотой.
\end{homeworkProblem}
\end{document}
