import numpy as np


def get_lpx_d_all(X, F, B, s):
    """
    Calculates log(p(X_k|d_k,F,B,s)) for all images X_k in X and
    all possible displacements d_k.
    Input parameters:
      X ... H x W x N numpy.array, N images of size H x W
      F ... h x w numpy.array, estimate of villain's face
      B ... H x W numpy.array, estimate of background
      s ... 1 x 1, estimate of standard deviation of Gaussian noise
    Output parameters:
      lpx_d_all ... (H-h+1) x (W-w+1) x N numpy.array,
                    px_d_all[dh,dw,k] - log-likelihood of
                    observing image X_k given that the villain's
                face F is located at displacement (dh, dw)
    """
    H, W, N = X.shape
    h, w = F.shape
    lpx_d_all = np.zeros((H-h+1, W-w+1, N))
    for dh in range(H - h + 1):
        for dw in range(W - w + 1):
            mu = B.copy()
            mu[dh:dh + h, dw:dw + w] = F
            lpx_d_all[dh, dw] -= (np.sum((X - mu[:,:,np.newaxis]) ** 2, axis=(0,1))) / (2 * s**2)
    lpx_d_all -= H*W*(0.5*np.log(2*np.pi) + np.log(s))
    return lpx_d_all


def calc_L(X, F, B, s, A, q, useMAP = False):
    """
    Calculates the lower bound L(q,F,B,s,A) for the marginal log
    likelihood

    Input parameters:

      X ... H x W x N numpy.array, N images of size H x W
      F ... h x w numpy.array, estimate of villain's face
      B ... H x W numpy.array, estimate of background
      s ... 1 x 1, estimate of standard deviation of Gaussian noise
      A ... (H-h+1) x (W-w+1) numpy.array, estimate of prior on
            displacement of face in any image
      q  ... if useMAP = False:
                (H-h+1) x (W-w+1) x N numpy.array,
                q[dh,dw,k] - estimate of posterior of displacement
                (dh,dw) of villain's face given image Xk
              if useMAP = True:
                2 x N numpy.array,
                q[1,k] - MAP estimates of dh for X_k
                q[2,k] - MAP estimates of dw for X_k
      useMAP ... logical, if true then q is a MAP estimates of
                 displacement (dh,dw) of villain's face given image
                 Xk

    Output parameters:

      L ... 1 x 1, the lower bound L(q,F,B,s,A) for the marginal log
            likelihood
    """
    if useMAP is False:
        e_pxd = np.sum((get_lpx_d_all(X, F, B, s) + np.log(A + 1e-7)[:,:,np.newaxis]) * q)
        e_qd = np.sum(np.ma.log(q) * q)
        return e_pxd - e_qd
    else:
        indices = q[0], q[1], np.arange(q.shape[1])
        return np.sum(get_lpx_d_all(X, F, B, s)[indices] + np.log(A + 1e-7)[q[0], q[1]])


def e_step(X, F, B, s, A, useMAP = False):
    """
    Given the current estimate of the parameters, for each image Xk
    estimates the probability p(d_k|X_k,F,B,s,A)
    Input parameters:
      X ... H x W x N numpy.array, N images of size H x W
      F ... h x w numpy.array, estimate of villain's face
      B ... H x W numpy.array, estimate of background
      s ... 1 x 1, estimate of standard deviation of Gaussian noise
      A ... (H-h+1) x (W-w+1) numpy.array, estimate of prior on
            displacement of face in any image
      useMAP ... logical, if true then q is a MAP estimates of
                 displacement (dh,dw) of villain's face given image
                 Xk
    Output parameters:
      q  ... if useMAP = False:
                (H-h+1) x (W-w+1) x N numpy.array,
                q[dh,dw,k] - estimate of posterior of displacement
                (dh,dw) of villain's face given image Xk
              if useMAP = True:
                2 x N numpy.array,
                q[1,k] - MAP estimates of dh for X_k
                q[2,k] - MAP estimates of dw for X_k
    """
    log_pxd = get_lpx_d_all(X, F, B, s) + np.log(A + 1e-7)[:,:,np.newaxis]
    log_pxd -= np.max(log_pxd, axis=(0, 1))
    pxd = np.exp(log_pxd)
    pd_x = pxd / np.sum(pxd, axis=(0, 1))
    if useMAP is False:
        return pd_x
    else:
        q = np.empty((2, X.shape[2]), dtype=np.int)
        for i in range(q.shape[1]):
            q[:,i] = np.unravel_index(pd_x[:,:,i].argmax(), pd_x.shape[:-1])
        return q


def m_step(X, q, h, w, useMAP = False):
    """
    Estimates F,B,s,A given estimate of posteriors defined by q

    Input parameters:

      X     ... H x W x N numpy.array, N images of size H x W
      q  ... if useMAP = False:
                (H-h+1) x (W-w+1) x N numpy.array,
                q[dh,dw,k] - estimate of posterior of displacement
                (dh,dw) of villain's face given image Xk
              if useMAP = True:
                2 x N numpy.array,
                q[1,k] - MAP estimates of dh for X_k
                q[2,k] - MAP estimates of dw for X_k
      h ... 1 x 1, face mask hight
      w ... 1 x 1, face mask width
      useMAP ... logical, if true then q is a MAP estimates of
                displacement (dh,dw) of villain's face given image
                Xk

    Output parameters:

      F ... h x w numpy.array, estimate of villain's face
      B ... H x W numpy.array, estimate of background
      s ... 1 x 1, estimate of standard deviation of Gaussian noise
      A ... (H-h+1) x (W-w+1) numpy.array, estimate of prior on
            displacement of face in any image
    """
    H, W, N = X.shape
    if useMAP is False:
        A = np.sum(q, axis=-1)
        A /= N
        F = np.empty((h, w))
        for i in range(h):
            for j in range(w):
                F[i, j] = np.sum(q * X[i:i+q.shape[0], j:j+q.shape[1]])
        F /= N
        q_masked = np.ma.array(q, mask=False)
        B = np.empty((H, W))
        for i in range(H):
            for j in range(W):
                q_masked.mask[max(i-h+1,0):i+1, max(j-w+1,0):j+1] = True
                B[i, j] = np.sum(q_masked * X[i, j]) / np.sum(q_masked)
                q_masked.mask[max(i-h+1,0):i+1, max(j-w+1,0):j+1] = False
        s = 0
        for dh in range(H - h + 1):
            for dw in range(W - w + 1):
                mu = B.copy()
                mu[dh:dh + h, dw:dw + w] = F
                s += np.sum(q[dh, dw] * (np.sum((X - mu[:,:,np.newaxis]) ** 2, axis=(0,1))))
        s /= N * H * W
        s **= 0.5
    else:
        A = np.zeros((H - h + 1, W - w + 1))
        for i, j in zip(q[0], q[1]):
            A[i,j] += 1
        A /= N
        F = np.zeros((h, w))
        for k, (i, j) in enumerate(zip(q[0], q[1])):
            F += X[i:i+h, j:j+w, k]
        F /= N
        B = np.zeros((H, W))
        B_norm = np.zeros((H, W))
        for k, (dh, dw) in enumerate(zip(q[0], q[1])):
            B += X[:,:,k]
            B[dh:dh+h,dw:dw+w] -= X[dh:dh+h,dw:dw+w,k]
            B_norm += 1
            B_norm[dh:dh+h,dw:dw+w] -= 1
        B[B_norm != 0] /= B_norm[B_norm != 0]
        s = 0
        for k, (dh, dw) in enumerate(zip(q[0], q[1])):
            mu = B.copy()
            mu[dh:dh + h, dw:dw + w] = F
            s += np.sum((X[:,:,k] - mu) ** 2)
        s /= N * H * W
        s **= 0.5
    return F, B, s, A


def gen_params(H, W, h, w):
    A = np.random.rand(H - h + 1, W - w + 1)
    A /= A.shape[0] * A.shape[1]
    F = np.random.rand(h, w) * 255
    B = np.random.rand(H, W) * 255
    s = np.random.rand() * 255
    return F, B, s, A


def run_EM(X, h, w, F=None, B=None, s=None, A=None,
    tolerance=0.001, max_iter=10, useMAP=False):
    """
    Runs EM loop until the likelihood of observing X given current
    estimate of parameters is idempotent as defined by a fixed
    tolerance

    Input parameters:

      X ... H x W x N numpy.array, N images of size H x W
      h ... 1 x 1, face mask hight
      w ... 1 x 1, face mask widht
      F, B, s, A ... initial parameters (optional!)
      F ... h x w numpy.array, estimate of villain's face
      B ... H x W numpy.array, estimate of background
      s ... 1 x 1, estimate of standart deviation of Gaussian noise
      A ... (H-h+1) x (W-w+1) numpy.array, estimate of prior on
            displacement of face in any image
      tolerance ... parameter for stopping criterion
      max_iter  ... maximum number of iterations
      useMAP ... logical, if true then after E-step we take only
                 MAP estimates of displacement (dh,dw) of villain's
                 face given image Xk


    Output parameters:

      F, B, s, A ... trained parameters
      LL ... 1 x (number_of_iters + 2) numpy.array, L(q,F,B,s,A)
             at initial guess, after each EM iteration and after
             final estimate of posteriors;
             number_of_iters is actual number of iterations that was
             done
    """
    params = gen_params(X.shape[0], X.shape[1], h, w)
    if F is None:
        F = params[0]
    if B is None:
        B = params[1]
    if s is None:
        s = params[2]
    if A is None:
        A = params[3]
    LL = np.empty(0)
    q = e_step(X, F, B, s, A, useMAP)
    LL = np.append(LL, calc_L(X, F, B, s, A, q, useMAP))
    while True:
        F, B, s, A = m_step(X, q, h, w, useMAP)
        q = e_step(X, F, B, s, A, useMAP)
        LL = np.append(LL, calc_L(X, F, B, s, A, q, useMAP))
        if LL[-1] - LL[-2] < tolerance:
            break
        if LL.shape[0] > max_iter:
            break

    F, B, s, A = m_step(X, q, h, w, useMAP)
    q = e_step(X, F, B, s, A, useMAP)
    LL = np.append(LL, calc_L(X, F, B, s, A, q, useMAP))
    return F, B, s, A, LL



def run_EM_with_restarts(X, h, w, tolerance=0.001, max_iter=50,
                     useMAP=False, restart=10):
    """
    Restarts EM several times from different random initializations
    and stores the best estimate of the parameters as measured by
    the L(q,F,B,s,A)

    Input parameters:

      X ... H x W x N numpy.array, N images of size H x W
      h ... 1 x 1, face mask hight
      w ... 1 x 1, face mask widht
      tolerance, max_iter, useMAP ... parameters for EM
      restart   ... number of EM runs

    Output parameters:

      F ... h x w numpy.array, the best estimate of villain's face
      B ... H x W numpy.array, the best estimate of background
      s ... 1 x 1, the best estimate of standart deviation of
            Gaussian noise
      A ... (H-h+1) x (W-w+1) numpy.array, the best estimate of
            prior on displacement of face in any image
      LL ... 1 x 1, the best L(q,F,B,s,A)
    """
    H, W = X.shape[:-1]
    if restart == 0:
        raise ValueError("restart value has to be greater than 0!")
    best_params = run_EM(X, h, w, *gen_params(H, W, h, w), tolerance=tolerance, max_iter=max_iter, useMAP=useMAP)
    for i in range(restart - 1):
        new_params = run_EM(X, h, w, *gen_params(H, W, h, w), tolerance=tolerance, max_iter=max_iter, useMAP=useMAP)
        if new_params[-1][-1] > best_params[-1][-1]:
            best_params = new_params
    return best_params

